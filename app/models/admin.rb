class Admin < ApplicationRecord
  belongs_to :trabajador, :foreign_key => :admin_id, :primary_key => :trabajador_rut
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
