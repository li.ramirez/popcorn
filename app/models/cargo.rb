class Cargo < ApplicationRecord
  #self.primary_key = :car_id
  has_many :trabajador, :foreign_key => :tra_car, :primary_key => :car_id

  validates :car_id , uniqueness: true
end
