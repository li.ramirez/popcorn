class Genero < ApplicationRecord
  has_many :pelicula, :foreign_key => :peli_gen, :primary_key => :gen_id

  validates :gen_id , uniqueness: true
end
