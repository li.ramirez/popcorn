class Pelicula < ApplicationRecord
  belongs_to :edad, :foreign_key => :peli_ed, :primary_key => :ed_id
  belongs_to :genero, :foreign_key => :peli_gen, :primary_key => :gen_id
  belongs_to :pais_origen, :foreign_key => :peli_po, :primary_key => :po_id
  has_many :funcion, :foreign_key => :fun_peli, :primary_key => :peli_id

  validates :peli_id , uniqueness: true
end
