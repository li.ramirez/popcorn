class EstTick < ApplicationRecord
  has_many :ticket, :foreign_key => :tick_et, :primary_key => :et_id

  validates :et_id , uniqueness: true
end
