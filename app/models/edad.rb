class Edad < ApplicationRecord
  has_many :pelicula, :foreign_key => :peli_ed, :primary_key => :ed_id

  validates :ed_id , uniqueness: true
end
