class Ticket < ApplicationRecord
  belongs_to :tipo_ticket, :foreign_key => :tick_tt, :primary_key => :tt_id
  belongs_to :funcion, :foreign_key => :tick_fun, :primary_key => :fun_id
  belongs_to :est_tick, :foreign_key => :tick_et, :primary_key => :et_id

  validates :tick_id , uniqueness: true
end
