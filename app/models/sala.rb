class Sala < ApplicationRecord
  belongs_to :tipo_sala, :foreign_key => :sala_ts, :primary_key => :ts_id
  belongs_to :cine, :foreign_key => :sala_cine, :primary_key => :cine_id
  has_many :funcion, :foreign_key => :fun_sala, :primary_key => :sala_id

  validates :sala_id , uniqueness: true
end
