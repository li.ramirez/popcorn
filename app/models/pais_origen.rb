class PaisOrigen < ApplicationRecord
  has_many :pelicula, :foreign_key => :peli_po, :primary_key => :po_id

  validates :po_id , uniqueness: true
end
