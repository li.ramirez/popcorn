class TipoSala < ApplicationRecord
  has_many :sala, :foreign_key => :sala_ts, :primary_key => :ts_id

  validates :ts_id , uniqueness: true
end
