class Horario < ApplicationRecord
  has_many :funcion, :foreign_key => :fun_hor, :primary_key => :hor_id

  validates :hor_id , uniqueness: true
end
