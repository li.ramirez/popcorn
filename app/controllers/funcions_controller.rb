class FuncionsController < ApplicationController
  before_action :set_funcion, only: [:show, :edit, :update, :destroy]

  # GET /funcions
  def index
    @funcions = Funcion.all
    @funcion = Funcion.new
  end

  # GET /funcions/1
  def show
  end

  # GET /funcions/new
  def new
  end

  # GET /funcions/1/edit
  def edit
  end

  # POST /funcions
  def create
    @funcion = Funcion.new(funcion_params)

    if @funcion.save
      redirect_to @funcion, notice: 'Funcion ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /funcions/1
  def update
    if @funcion.update(funcion_params)
      redirect_to @funcion, notice: 'Funcion ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /funcions/1
  def destroy
    @funcion.destroy
    redirect_to funcions_url, notice: 'Funcion was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_funcion
      @funcion = Funcion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def funcion_params
      params.require(:funcion).permit(:fun_fecha, :fun_sala, :fun_ef, :fun_peli, :fun_hor)
    end
end
