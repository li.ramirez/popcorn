class PaisOrigensController < ApplicationController
  before_action :set_pais_origen, only: [:show, :edit, :update, :destroy]

  # GET /pais_origens
  def index
    @pais_origens = PaisOrigen.all
  end

  # GET /pais_origens/1
  def show
  end

  # GET /pais_origens/new
  def new
    @pais_origen = PaisOrigen.new
  end

  # GET /pais_origens/1/edit
  def edit
  end

  # POST /pais_origens
  def create
    #@pais_origen = PaisOrigen.new(pais_origen_params)

    @id = params[:pais_origen][:po_id]
    @desc = params[:pais_origen][:po_nom]
    @trans = params[:pais_origen][:trans]
    plsql.PRO_MANT_PAIS_ORIGEN(@id, @desc, @trans)

    redirect_to page_mtb_path, notice: 'Pais origen ha sido agregado exitosamente.'

    #if @pais_origen.save

    #else
    #  redirect_to page_mtb_path, notice: 'Pais origen no se pudo agregar.'
    #end
  end

  # PATCH/PUT /pais_origens/1
  def update
    #if @pais_origen.update(pais_origen_params)



    redirect_to page_mtb_path, notice: 'Pais origen ha sido actualizado.'
    #else
    #  redirect_to page_mtb_path, notice: 'Pais origen no se pudo actualizar.'
    #end
  end

  # DELETE /pais_origens/1
  def destroy
    #@pais_origen.destroy
    @id = params[:pais_origen][:po_id]
    @desc = params[:pais_origen][:po_nom]
    plsql.PRO_MANT_PAIS_ORIGEN(@id, @desc, "E")
    redirect_to page_mtb_path, notice: 'Pais origen ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pais_origen
      @pais_origen = PaisOrigen.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pais_origen_params
      params.require(:pais_origen).permit(:po_id, :po_nom, :trans)
    end
end
