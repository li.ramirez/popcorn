class SalasController < ApplicationController
  before_action :set_sala, only: [:show, :edit, :update, :destroy]

  # GET /salas
  def index
    @salas = Sala.all
    @sala = Sala.new
  end

  # GET /salas/1
  def show
  end

  # GET /salas/new
  def new
    @sala = Sala.new
  end

  # GET /salas/1/edit
  def edit
  end

  # POST /salas
  def create
    @sala = Sala.new(sala_params)

    if @sala.save
      redirect_to @sala, notice: 'Sala ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /salas/1
  def update
    if @sala.update(sala_params)
      redirect_to @sala, notice: 'Sala ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /salas/1
  def destroy
    @sala.destroy
    redirect_to salas_url, notice: 'Sala was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sala
      @sala = Sala.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sala_params
      params.require(:sala).permit(:sala_cap, :sala_ts, :sala_cine)
    end
end
