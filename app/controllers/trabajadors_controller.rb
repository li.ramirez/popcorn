class TrabajadorsController < ApplicationController
  before_action :set_trabajador, only: [:show, :edit, :update, :destroy]

  # GET /trabajadors
  def index
    @trabajadors = Trabajador.all
    @trabajador = Trabajador.new
  end

  # GET /trabajadors/1
  def show
  end

  # GET /trabajadors/new
  def new
  end

  # GET /trabajadors/1/edit
  def edit
  end

  # POST /trabajadors
  def create
    @trabajador = Trabajador.new(trabajador_params)

    if @trabajador.save
      redirect_to @trabajador, notice: 'Trabajador ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /trabajadors/1
  def update
    if @trabajador.update(trabajador_params)
      redirect_to @trabajador, notice: 'Trabajador ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /trabajadors/1
  def destroy
    @trabajador.destroy
    redirect_to trabajadors_url, notice: 'Trabajador was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trabajador
      @trabajador = Trabajador.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def trabajador_params
      params.require(:trabajador).permit(:tra_rut, :tra_nom, :tra_apat, :tra_amat, :tra_direc, :tra_fono, :tra_mail, :tra_pass, :tra_car, :tra_cine)
    end
end
