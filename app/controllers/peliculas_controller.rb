class PeliculasController < ApplicationController
  before_action :set_pelicula, only: [:show, :edit, :update, :destroy]

  # GET /peliculas
  def index
    @peliculas = Pelicula.all
    @pelicula = Pelicula.new
  end

  # GET /peliculas/1
  def show
  end

  # GET /peliculas/new
  def new
    @pelicula = Pelicula.new
  end

  # GET /peliculas/1/edit
  def edit
  end

  # POST /peliculas
  def create
    @pelicula = Pelicula.new(pelicula_params)

    if @pelicula.save
      redirect_to @pelicula, notice: 'Pelicula ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /peliculas/1
  def update
    if @pelicula.update(pelicula_params)
      redirect_to @pelicula, notice: 'Pelicula ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /peliculas/1
  def destroy
    @pelicula.destroy
    redirect_to peliculas_url, notice: 'Pelicula was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pelicula
      @pelicula = Pelicula.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pelicula_params
      params.require(:pelicula).permit( :peli_nom, :peli_direc, :peli_fest, :peli_sinop, :peli_dura, :peli_gen, :peli_ed, :peli_po)
    end
end
