class HorariosController < ApplicationController
  before_action :set_horario, only: [:show, :edit, :update, :destroy]

  # GET /horarios
  def index
    @horarios = Horario.all
  end

  # GET /horarios/1
  def show
  end

  # GET /horarios/new
  def new
    @horario = Horario.new
  end

  # GET /horarios/1/edit
  def edit
  end

  # POST /horarios
  def create
    @horario = Horario.new(horario_params)

    if @horario.save
      redirect_to @horario, notice: 'Horario ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Horario no se pudo agregar.'
    end
  end

  # PATCH/PUT /horarios/1
  def update
    if @horario.update(horario_params)
      redirect_to @horario, notice: 'Horario ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Horario no se pudo actualizar.'
    end
  end

  # DELETE /horarios/1
  def destroy
    @horario.destroy
    redirect_to page_mtb_path, notice: 'Horario ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_horario
      @horario = Horario.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def horario_params
      params.require(:horario).permit(:hor_id, :hor_desc)
    end
end
