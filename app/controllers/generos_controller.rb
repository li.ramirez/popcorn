class GenerosController < ApplicationController
  before_action :set_genero, only: [:show, :edit, :update, :destroy]

  # GET /generos
  def index
    @generos = Genero.all
  end

  # GET /generos/1
  def show
  end

  # GET /generos/new
  def new
    @genero = Genero.new
  end

  # GET /generos/1/edit
  def edit
  end

  # POST /generos
  def create
    @genero = Genero.new(genero_params)

    if @genero.save
      redirect_to @genero, notice: 'Genero ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Genero no se pudo agregar.'
    end
  end

  # PATCH/PUT /generos/1
  def update
    if @genero.update(genero_params)
      redirect_to @genero, notice: 'Genero ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Genero no se pudo actualizar.'
    end
  end

  # DELETE /generos/1
  def destroy
    @genero.destroy
    redirect_to page_mtb_path, notice: 'Genero ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_genero
      @genero = Genero.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def genero_params
      params.require(:genero).permit(:gen_id, :gen_desc)
    end
end
