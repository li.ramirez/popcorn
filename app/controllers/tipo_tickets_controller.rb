class TipoTicketsController < ApplicationController
  before_action :set_tipo_ticket, only: [:show, :edit, :update, :destroy]

  # GET /tipo_tickets
  def index
    @tipo_tickets = TipoTicket.all
  end

  # GET /tipo_tickets/1
  def show
  end

  # GET /tipo_tickets/new
  def new
    @tipo_ticket = TipoTicket.new
  end

  # GET /tipo_tickets/1/edit
  def edit
  end

  # POST /tipo_tickets
  def create
    @tipo_ticket = TipoTicket.new(tipo_ticket_params)

    if @tipo_ticket.save
      redirect_to page_mtb_path, notice: 'Tipo ticket ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Tipo ticket no se pudo agregar.'
    end
  end

  # PATCH/PUT /tipo_tickets/1
  def update
    if @tipo_ticket.update(tipo_ticket_params)
      redirect_to page_mtb_path, notice: 'Tipo ticket ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Tipo ticket no se pudo actualizar.'
    end
  end

  # DELETE /tipo_tickets/1
  def destroy
    @tipo_ticket.destroy
    redirect_to page_mtb_path, notice: 'Tipo ticket ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipo_ticket
      @tipo_ticket = TipoTicket.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def tipo_ticket_params
      params.require(:tipo_ticket).permit(:tt_id, :tt_desc)
    end
end
