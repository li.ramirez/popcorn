class EstFunsController < ApplicationController
  before_action :set_est_fun, only: [:show, :edit, :update, :destroy]

  # GET /est_funs
  def index
    @est_funs = EstFun.all
  end

  # GET /est_funs/1
  def show
  end

  # GET /est_funs/new
  def new
    @est_fun = EstFun.new
  end

  # GET /est_funs/1/edit
  def edit
  end

  # POST /est_funs
  def create
    @est_fun = EstFun.new(est_fun_params)

    if @est_fun.save
      redirect_to @est_fun, notice: 'Est fun ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Est fun no se pudo agregar.'
    end
  end

  # PATCH/PUT /est_funs/1
  def update
    if @est_fun.update(est_fun_params)
      redirect_to @est_fun, notice: 'Est fun ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Est fun no se pudo actualizar.'
    end
  end

  # DELETE /est_funs/1
  def destroy
    @est_fun.destroy
    redirect_to page_mtb_path, notice: 'Est fun ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_est_fun
      @est_fun = EstFun.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def est_fun_params
      params.require(:est_fun).permit(:ef_id, :ef_desc)
    end
end
