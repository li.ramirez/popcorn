class CinesController < ApplicationController
  before_action :set_cine, only: [:show, :edit, :update, :destroy]

  # GET /cines
  def index
    @cines = Cine.all
    @cine = Cine.new
  end

  # GET /cines/1
  def show
  end

  # GET /cines/new
  def new
  end

  # GET /cines/1/edit
  def edit
  end

  # POST /cines
  def create
    @cine = Cine.new(cine_params)

    if @cine.save
      redirect_to @cine, notice: 'Cine ha sido agregado exitosamente.'
    else
      render :new
    end
  end

  # PATCH/PUT /cines/1
  def update
    if @cine.update(cine_params)
      redirect_to @cine, notice: 'Cine ha sido actualizado.'
    else
      render :edit
    end
  end

  # DELETE /cines/1
  def destroy
    @cine.destroy
    redirect_to cines_url, notice: 'Cine was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cine
      @cine = Cine.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def cine_params
      params.require(:cine).permit(:cine_nom, :cine_direc, :cine_fono)
    end
end
