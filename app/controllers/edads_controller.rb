class EdadsController < ApplicationController
  before_action :set_edad, only: [:show, :edit, :update, :destroy]

  # GET /edads
  def index
    @edads = Edad.all
  end

  # GET /edads/1
  def show
  end

  # GET /edads/new
  def new
    @edad = Edad.new
  end

  # GET /edads/1/edit
  def edit
  end

  # POST /edads
  def create
    @edad = Edad.new(edad_params)

    if @edad.save
      redirect_to @edad, notice: 'Edad ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Edad no se pudo agregar.'
    end
  end

  # PATCH/PUT /edads/1
  def update
    if @edad.update(edad_params)
      redirect_to @edad, notice: 'Edad ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Edad no se pudo actualizar.'
    end
  end

  # DELETE /edads/1
  def destroy
    @edad.destroy
    redirect_to page_mtb_path, notice: 'Edad ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_edad
      @edad = Edad.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def edad_params
      params.require(:edad).permit( :ed_id, :ed_desc)
    end
end
