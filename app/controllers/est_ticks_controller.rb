class EstTicksController < ApplicationController
  before_action :set_est_tick, only: [:show, :edit, :update, :destroy]

  # GET /est_ticks
  def index
    @est_ticks = EstTick.all
  end

  # GET /est_ticks/1
  def show
  end

  # GET /est_ticks/new
  def new
    @est_tick = EstTick.new
  end

  # GET /est_ticks/1/edit
  def edit
  end

  # POST /est_ticks
  def create
    @est_tick = EstTick.new(est_tick_params)

    if @est_tick.save
      redirect_to @est_tick, notice: 'Est tick ha sido agregado exitosamente.'
    else
      redirect_to page_mtb_path, notice: 'Est tick no se pudo agregar.'
    end
  end

  # PATCH/PUT /est_ticks/1
  def update
    if @est_tick.update(est_tick_params)
      redirect_to @est_tick, notice: 'Est tick ha sido actualizado.'
    else
      redirect_to page_mtb_path, notice: 'Est tick no se pudo actualizar.'
    end
  end

  # DELETE /est_ticks/1
  def destroy
    @est_tick.destroy
    redirect_to page_mtb_path, notice: 'Est tick ha sido eliminado.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_est_tick
      @est_tick = EstTick.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def est_tick_params
      params.require(:est_tick).permit(:et_id, :et_desc)
    end
end
