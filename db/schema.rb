# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170702010713) do

  create_table "admin", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: 6
    t.datetime "remember_created_at", precision: 6
    t.integer "sign_in_count", precision: 38, default: 0, null: false
    t.datetime "current_sign_in_at", precision: 6
    t.datetime "last_sign_in_at", precision: 6
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_admin_on_email", unique: true
    t.index ["reset_password_token"], name: "i_admin_reset_password_token", unique: true
  end

  create_table "audit_tables", id: false, force: :cascade do |t|
    t.string "aud_trans", limit: 1, null: false
    t.string "aud_table", limit: 50, null: false
    t.date "aud_fecha", null: false
    t.string "aud_user", limit: 20, null: false
    t.integer "aud_tabla_id", limit: 4, precision: 4, null: false
    t.string "aud_nuevo", limit: 50
    t.string "aud_antiguo", limit: 50
  end

  create_table "cargo", primary_key: "car_id", id: :string, limit: 4, comment: "Identificador", force: :cascade, comment: "Cargo de los trabajadores." do |t|
    t.string "car_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "cine", primary_key: "cine_id", force: :cascade, comment: "Cines." do |t|
    t.string "cine_nom", limit: 30, null: false, comment: "Nombre"
    t.string "cine_direc", limit: 100, null: false, comment: "Direccion"
    t.string "cine_fono", limit: 12, comment: "Telefono con codigo de area"
  end

  create_table "edad", primary_key: "ed_id", id: :string, limit: 5, comment: "Identificador", force: :cascade, comment: "Clasificacion de edad." do |t|
    t.string "ed_desc", limit: 100, null: false, comment: "Descripcion"
  end

  create_table "est_fun", primary_key: "ef_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Estado de la funcion." do |t|
    t.string "ef_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "est_tick", primary_key: "et_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Estado del ticket" do |t|
    t.string "et_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "funcion", primary_key: "fun_id", force: :cascade, comment: "Funciones." do |t|
    t.date "fun_fecha", null: false, comment: "Fecha"
    t.integer "fun_sala", limit: 2, precision: 2, null: false, comment: "Sala"
    t.string "fun_ef", limit: 3, null: false, comment: "Estado"
    t.integer "fun_peli", limit: 4, precision: 4, null: false, comment: "Pelicula"
    t.string "fun_hor", limit: 2, null: false, comment: "Horario"
  end

  create_table "genero", primary_key: "gen_id", id: :string, limit: 20, comment: "Identificador", force: :cascade, comment: "Genero principal al que pertenece la pelicula." do |t|
    t.string "gen_desc", limit: 500, null: false, comment: "Descripcion"
  end

  create_table "horario", primary_key: "hor_id", id: :string, limit: 2, comment: "Identificador", force: :cascade, comment: "Horario de la funcion." do |t|
    t.string "hor_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "pais_origen", primary_key: "po_id", id: :string, limit: 3, comment: "Identificador", force: :cascade, comment: "Pais de origen de la pelicula." do |t|
    t.string "po_nom", limit: 20, null: false, comment: "Nombre"
  end

  create_table "pelicula", primary_key: "peli_id", force: :cascade, comment: "Peliculas." do |t|
    t.string "peli_nom", limit: 50, null: false, comment: "Nombre"
    t.string "peli_direc", limit: 30, null: false, comment: "Director"
    t.date "peli_fest", null: false, comment: "Fecha de estreno"
    t.string "peli_sinop", limit: 1000, comment: "Sinopsis"
    t.integer "peli_dura", limit: 3, precision: 3, null: false, comment: "Tiempo de duracion en minutos"
    t.string "peli_gen", limit: 20, null: false, comment: "Genero principal"
    t.string "peli_ed", limit: 5, null: false, comment: "Clasificacion de edad"
    t.string "peli_po", limit: 3, null: false, comment: "Pais de origen"
  end

  create_table "resumen_ticket", primary_key: "rst_cine_id", force: :cascade do |t|
    t.string "rst_cine_nom", limit: 30, null: false
    t.decimal "rst_cajeros", null: false
    t.decimal "rst_vendidos", null: false
    t.decimal "rst_dev", null: false
    t.decimal "rst_dias", null: false
    t.string "rst_usuario", limit: 20, null: false
    t.string "rst_ip", limit: 20, null: false
    t.date "rst_fecha", null: false
  end

  create_table "sala", primary_key: "sala_id", force: :cascade, comment: "Salas." do |t|
    t.integer "sala_cap", limit: 2, precision: 2, null: false, comment: "Cantidad de asientos"
    t.string "sala_ts", limit: 2, null: false, comment: "Acondicionamiento de la sala"
    t.integer "sala_cine", limit: 3, precision: 3, null: false, comment: "Cine al que pertenece"
  end

  create_table "ticket", primary_key: "tick_id", force: :cascade, comment: "Tickets." do |t|
    t.date "tick_fecha", null: false, comment: "Fecha de emision"
    t.string "tick_tt", limit: 4, null: false, comment: "Tipo"
    t.integer "tick_fun", limit: 3, precision: 3, null: false, comment: "Funcion"
    t.string "tick_et", limit: 3, null: false, comment: "Estado en el que se ecuentra"
  end

  create_table "tipo_sala", primary_key: "ts_id", id: :string, limit: 2, comment: "Identificador", force: :cascade, comment: "Tipo de acondicionamiento de la sala." do |t|
    t.string "ts_desc", limit: 60, null: false, comment: "Descripcion"
  end

  create_table "tipo_ticket", primary_key: "tt_id", id: :string, limit: 4, comment: "Identficador", force: :cascade, comment: "Tipo de ticket." do |t|
    t.string "tt_desc", limit: 30, null: false, comment: "Descripcion"
  end

  create_table "trabajador", primary_key: "tra_rut", force: :cascade, comment: "Trabajadores." do |t|
    t.string "tra_nom", limit: 20, null: false, comment: "Nombre"
    t.string "tra_apat", limit: 20, null: false, comment: "Apellido paterno"
    t.string "tra_amat", limit: 20, null: false, comment: "Apellido materno"
    t.string "tra_direc", limit: 100, null: false, comment: "Direccion"
    t.string "tra_fono", limit: 12, null: false, comment: "Telefono con codigo de area"
    t.string "tra_mail", limit: 50, comment: "Correo electronico"
    t.string "tra_pass", limit: 50, comment: "Contrase?a"
    t.string "tra_car", limit: 4, null: false, comment: "Cargo"
    t.integer "tra_cine", limit: 3, precision: 3, null: false, comment: "Cine"
  end

  add_foreign_key "funcion", "est_fun", column: "fun_ef", primary_key: "ef_id", name: "fk_fun_ef"
  add_foreign_key "funcion", "horario", column: "fun_hor", primary_key: "hor_id", name: "fk_fun_hor"
  add_foreign_key "funcion", "pelicula", column: "fun_peli", primary_key: "peli_id", name: "fk_fun_peli"
  add_foreign_key "funcion", "sala", column: "fun_sala", primary_key: "sala_id", name: "fk_fun_sala"
  add_foreign_key "pelicula", "edad", column: "peli_ed", primary_key: "ed_id", name: "fk_peli_ed"
  add_foreign_key "pelicula", "genero", column: "peli_gen", primary_key: "gen_id", name: "fk_peli_gen"
  add_foreign_key "pelicula", "pais_origen", column: "peli_po", primary_key: "po_id", name: "fk_peli_po"
  add_foreign_key "sala", "cine", column: "sala_cine", primary_key: "cine_id", name: "fk_sala_cine"
  add_foreign_key "sala", "tipo_sala", column: "sala_ts", primary_key: "ts_id", name: "fk_sala_ts"
  add_foreign_key "ticket", "est_tick", column: "tick_et", primary_key: "et_id", name: "fk_tick_et"
  add_foreign_key "ticket", "funcion", column: "tick_fun", primary_key: "fun_id", name: "fk_tick_fun"
  add_foreign_key "ticket", "tipo_ticket", column: "tick_tt", primary_key: "tt_id", name: "fk_tick_tt"
  add_foreign_key "trabajador", "cargo", column: "tra_car", primary_key: "car_id", name: "fk_tra_car"
  add_foreign_key "trabajador", "cine", column: "tra_cine", primary_key: "cine_id", name: "fk_tra_cine"
end
