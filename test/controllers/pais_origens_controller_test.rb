require 'test_helper'

class PaisOrigensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pais_origen = pais_origen(:one)
  end

  test "should get index" do
    get pais_origens_url
    assert_response :success
  end

  test "should get new" do
    get new_pais_origen_url
    assert_response :success
  end

  test "should create pais_origen" do
    assert_difference('PaisOrigen.count') do
      post pais_origens_url, params: { pais_origen: { po_nom: @pais_origen.po_nom } }
    end

    assert_redirected_to pais_origen_url(PaisOrigen.last)
  end

  test "should show pais_origen" do
    get pais_origen_url(@pais_origen)
    assert_response :success
  end

  test "should get edit" do
    get edit_pais_origen_url(@pais_origen)
    assert_response :success
  end

  test "should update pais_origen" do
    patch pais_origen_url(@pais_origen), params: { pais_origen: { po_nom: @pais_origen.po_nom } }
    assert_redirected_to pais_origen_url(@pais_origen)
  end

  test "should destroy pais_origen" do
    assert_difference('PaisOrigen.count', -1) do
      delete pais_origen_url(@pais_origen)
    end

    assert_redirected_to pais_origens_url
  end
end
