require 'test_helper'

class TipoTicketsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @tipo_ticket = tipo_ticket(:one)
  end

  test "should get index" do
    get tipo_tickets_url
    assert_response :success
  end

  test "should get new" do
    get new_tipo_ticket_url
    assert_response :success
  end

  test "should create tipo_ticket" do
    assert_difference('TipoTicket.count') do
      post tipo_tickets_url, params: { tipo_ticket: { tt_desc: @tipo_ticket.tt_desc } }
    end

    assert_redirected_to tipo_ticket_url(TipoTicket.last)
  end

  test "should show tipo_ticket" do
    get tipo_ticket_url(@tipo_ticket)
    assert_response :success
  end

  test "should get edit" do
    get edit_tipo_ticket_url(@tipo_ticket)
    assert_response :success
  end

  test "should update tipo_ticket" do
    patch tipo_ticket_url(@tipo_ticket), params: { tipo_ticket: { tt_desc: @tipo_ticket.tt_desc } }
    assert_redirected_to tipo_ticket_url(@tipo_ticket)
  end

  test "should destroy tipo_ticket" do
    assert_difference('TipoTicket.count', -1) do
      delete tipo_ticket_url(@tipo_ticket)
    end

    assert_redirected_to tipo_tickets_url
  end
end
