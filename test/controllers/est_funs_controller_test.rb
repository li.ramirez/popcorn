require 'test_helper'

class EstFunsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @est_fun = est_fun(:one)
  end

  test "should get index" do
    get est_funs_url
    assert_response :success
  end

  test "should get new" do
    get new_est_fun_url
    assert_response :success
  end

  test "should create est_fun" do
    assert_difference('EstFun.count') do
      post est_funs_url, params: { est_fun: { ef_desc: @est_fun.ef_desc } }
    end

    assert_redirected_to est_fun_url(EstFun.last)
  end

  test "should show est_fun" do
    get est_fun_url(@est_fun)
    assert_response :success
  end

  test "should get edit" do
    get edit_est_fun_url(@est_fun)
    assert_response :success
  end

  test "should update est_fun" do
    patch est_fun_url(@est_fun), params: { est_fun: { ef_desc: @est_fun.ef_desc } }
    assert_redirected_to est_fun_url(@est_fun)
  end

  test "should destroy est_fun" do
    assert_difference('EstFun.count', -1) do
      delete est_fun_url(@est_fun)
    end

    assert_redirected_to est_funs_url
  end
end
