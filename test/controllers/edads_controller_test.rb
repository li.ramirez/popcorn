require 'test_helper'

class EdadsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @edad = edad(:one)
  end

  test "should get index" do
    get edads_url
    assert_response :success
  end

  test "should get new" do
    get new_edad_url
    assert_response :success
  end

  test "should create edad" do
    assert_difference('Edad.count') do
      post edads_url, params: { edad: { ed_desc: @edad.ed_desc } }
    end

    assert_redirected_to edad_url(Edad.last)
  end

  test "should show edad" do
    get edad_url(@edad)
    assert_response :success
  end

  test "should get edit" do
    get edit_edad_url(@edad)
    assert_response :success
  end

  test "should update edad" do
    patch edad_url(@edad), params: { edad: { ed_desc: @edad.ed_desc } }
    assert_redirected_to edad_url(@edad)
  end

  test "should destroy edad" do
    assert_difference('Edad.count', -1) do
      delete edad_url(@edad)
    end

    assert_redirected_to edads_url
  end
end
