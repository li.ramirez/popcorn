require 'test_helper'

class PageControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get page_index_url
    assert_response :success
  end

  test "should get vista" do
    get page_vista_url
    assert_response :success
  end

  test "should get procedimiento" do
    get page_procedimiento_url
    assert_response :success
  end

  test "should get mtb" do
    get page_mtb_url
    assert_response :success
  end

end
