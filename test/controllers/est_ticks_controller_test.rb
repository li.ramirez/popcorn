require 'test_helper'

class EstTicksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @est_tick = est_tick(:one)
  end

  test "should get index" do
    get est_ticks_url
    assert_response :success
  end

  test "should get new" do
    get new_est_tick_url
    assert_response :success
  end

  test "should create est_tick" do
    assert_difference('EstTick.count') do
      post est_ticks_url, params: { est_tick: { et_desc: @est_tick.et_desc } }
    end

    assert_redirected_to est_tick_url(EstTick.last)
  end

  test "should show est_tick" do
    get est_tick_url(@est_tick)
    assert_response :success
  end

  test "should get edit" do
    get edit_est_tick_url(@est_tick)
    assert_response :success
  end

  test "should update est_tick" do
    patch est_tick_url(@est_tick), params: { est_tick: { et_desc: @est_tick.et_desc } }
    assert_redirected_to est_tick_url(@est_tick)
  end

  test "should destroy est_tick" do
    assert_difference('EstTick.count', -1) do
      delete est_tick_url(@est_tick)
    end

    assert_redirected_to est_ticks_url
  end
end
