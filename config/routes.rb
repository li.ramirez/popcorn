Rails.application.routes.draw do
  devise_for :admins
  resources :tickets
  resources :funcions
  resources :salas
  resources :trabajadors
  resources :pais_origens
  resources :cines
  resources :est_ticks
  resources :est_funs
  resources :horarios
  resources :tipo_tickets
  resources :tipo_salas
  resources :cargos
  resources :edads
  resources :peliculas
  resources :generos
  resources :pruebas

  get 'page/index'
  get 'page/vista'
  get 'page/procedimiento'
  get 'page/auditor'
  get 'page/mtb'
  get 'pages/index'
  get 'pages/vista'
  get 'pages/procedimiento'
  get 'pages/auditor'
  get 'pages/mtb'

  root 'page#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
